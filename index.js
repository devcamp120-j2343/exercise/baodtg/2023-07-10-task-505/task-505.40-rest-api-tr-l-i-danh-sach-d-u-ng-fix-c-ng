//Import thư viện expressjs
const express = require('express');

//khởi tạo 1 app express
const app = express();

//KHai báo cổng chạy project
const port = 8000;

//Khởi tạo class Drink
class Drink {
    constructor(id, drinkCode, drinkName, price, createDate, updateDate) {
        this.id = id,
            this.drinkCode = drinkCode,
            this.drinkName = drinkName,
            this.price = price,
            this.createDate = createDate,
            this.updateDate = updateDate

    }
}
//Khởi tạo drink object:
let drinkObj = [
    {
        id: 1,
        drinkCode: "TRATAC",
        drinkName: "Trà tắc",
        price: 10000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
    {
        id: 2,
        drinkCode: "COCA",
        drinkName: "Cocacola",
        price: 15000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
    {
        id: 3,
        drinkCode: "PEPSI",
        drinkName: "Pepsi",
        price: 15000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
]


//Callback function là 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
//khai báo API dạng /
//GET drink class
app.get("/drink-class", (req, res) => {

    let drinkClass1 = new Drink(1, "TRATAC", "Trà tắc", "10000", "14/05/2021", "14/05/2021");
    let drinkClass2 = new Drink(2, "COCA", "Cocacola", "15000", "14/05/2021", "14/05/2021");
    let drinkClass3 = new Drink(3, "PEPSI", "Pepsi", "15000", "14/05/2021", "14/05/2021");
    res.json({

        drinkClass1,
        drinkClass2,
        drinkClass3


    })
})

//GET drink object
app.get("/drink-object", (req, res) => {

    res.json({

        drinkObj


    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port)
})